## _Comment utiliser Shadow sur ChromeBook ?_ 
Pour utiliser Shadow sur Chromebook (intel/amd) , on a 3 solutions : 
- **Shadow via Crostini**
- **Shadowroot (Mode développeur)**
- **Shadow Mobile (téléchargeable via le Play Store)** 
>>> **100% Fonctionnel depuis la mise à jour Android 3.6.1**

>>> Durant le 2e Trimestre de 2021 , il y aura l'arrivée de Shadow Web App ouvrant alors des nouvelles portes sur la compatibilité chromebook & Shadow
 ## _Via Crostini(Linux Beta)_

1. Activer Crostini : https://support.google.com/chromebook/answer/9145439?hl=fr
2. Télécharger Shadow :  https://update.shadow.tech/launcher/prod/linux/ubuntu_18.04/Shadow.AppImage
3. Déplacer le fichier **Shadow.AppImage** dans le dossier : **Mes fichiers Linux**` 
4. Sur le terminal taper (ou copier-coller): 
```
sudo apt-get install -y --no-install-recommends \
    libva-glx2 \
    gnome-keyring \
    libnss3-dev \
    libinput10 \ 
    gconf2 \
    libgles2-mesa \
    libubsan0 \
    libuv1 
```

    
5. Pour finir taper sur le terminal : `chmod a+x Shadow.AppImage` puis `./Shadow.AppImage`
Ce dernier va se lancer et à son ouverture le gnome-keyring s'affichera choisisait un mot de passe quitte à ce que ça soit 
une lettre , le gnome-keyring est utile seulement pour retenir votre identifiant & mot de passe shadow afin d'éviter de le 
saisir à chaque fois :) 

> 6. Astuce : soit vous lancez à  chaque shadow via le terminal et la commande suivante : `./Shadow.AppImage` soit vous faites ceci un `sudo apt-get install menulibre `  puis taper `menulibre` . Il vous faut choisir une catégorie prenons par exemple **Other** , cliquer sur le petit **plus bleu** puis **Add Launcher** : il vous faut écrire à la place de de New Launcher : Shadow , vous pouvez allez sur google et télécharger une petite image de l'icone shadow que vous placerez dans mes fichiers linux et dans la ligne command il faut taper : ./Shadow.AppImage une fois ceci de fait , il ne vous reste plus qu'a sauvegardé en cliquant sur **save launcher a côté du petit plus** vous attendez un peu 10s et vous pouvez fermer cela. Et normalement vous devriez retrouver le raccourci shadow dans vos Apps Chromebooks !

7. Dans les paramètres : il faut aller cocher dans la catégorie Test : **Software Decoding**
8. Lancer votre Shadow & tout devrait rouler


 ## _Bugs Via Crostini(Linux Beta)_
02/12/2020 : A l'heure actuelle vous pouvez utiliser votre shadow pour n'importe quelle activitée sauf le GAMING ! **Pourquoi ?** Tout simplement parce que l'ancrage de la souris via le quick menu fonctionne pas très bien, le problème je l'ai signalé. 
Or si vous allez dans les paramètres de la souris est vous effectuez +/- ce paramètrage suivant , la souris une fois ancré s'améliore fortement: </p>
<div align="center"><a href=""><img src="https://zupimages.net/up/21/06/queg.png" height="300" width="300"  alt="" /></a></div>

## _Quelle solution pour le gaming ? (Temporaire)_
Je vous propose d'installer sur votre chromebook & votre shadow : **Rainway ** qui vous assure un support total **souris + manette **  

1. Télécharger sur shadow & installer la version windows 10 de rainway via le lien suivant : https://rainway.com/download
2. Incrivez vous & identifiez vous 
3. Sur Google Chrome de la même manière installer la web app rainway & identifiez- vous  : https://play.rainway.com/
4. Lancer Shadow sur Crostini puis lancer rainway sur chrome OS, de ce fait laisser tourner donc le shadow sur crostini et lancer le jeu que vous voulez via Rainway (grace à ce dernier on a souris + manette de fonctionnel en jeu)
5. Je vous conseil fortement de quand même réduire la bande passante de Shadow entre 5 & 20mbs & égalemment de réduire ou couper le son sur rainway pour éviter d'avoir du doublon ! Si non normalement si vous connexion tien la route , vous ne devriez pas voir de lag ou subir  des lags ! 

## _Shadowroot_

Rejoignez nous sur le discord : https://discord.com/invite/Zf4sbHu, tout est expliqué en détail avec en prime des astuces pour optimiser tout ce système et faire le moins de manips possible & vous pourrez avoir du support
vis à vis de Shadowroot 

 ## _Bugs Via Shadowroot_
Normalement tout est fonctionnel , n'hésitez pas à le signaler à gtx sur le discord shadow shade dans la section agora si vous en voyiez un ou si vous en trouvez un !

## _Quelques Raccourcit Claviers utiles_

-Touche Win : [ctrl] + [echap] <p>
-Système alt-tab : [ctrl]+[alt]+[tab]

## _Support Shadow_

- Support Communautaire : Shadow FR : https://discord.gg/shadowtech

- Support de Shadow : https://support.shadow.tech/hc/fr

## _Code Parrainage_

Pour toute nouvelle souscription à Shadow n'hésitez pas à utiliser le code suivante : `MATKEJWV` : https://shop.shadow.tech/invite/MATKEJWV
